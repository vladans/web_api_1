﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Schools.Models
{
    public class School
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Place { get; set; }
        public int Year { get; set; }

        public School(int id, string name, string place, int year)
        {
            this.Id = id;
            this.Name = name;
            this.Place = place;
            this.Year = year;
        }
    }
}