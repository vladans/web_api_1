﻿using Schools.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Schools.Controllers
{
    public class SchoolsController : ApiController
    {
        static IList<School> Schools = new List<School>()
        {
            new School(1, "Skola1", "Novi Sad", 1984),
            new School(2, "Skola2", "Sombor", 1990),
            new School(3, "Skola3", "Nis", 1954)
        };

        // GET api/<controller>
        // [Route("api/schools")]
        // [HttpGet]
        public IHttpActionResult GetAll()
        {
            if (Schools != null)
            {
                return Ok(Schools);
            }

            return NotFound();
        }

        // GET api/<controller>/id
        // [Route("api/schools/id")]
        // [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            var school = Schools.FirstOrDefault(x => x.Id == id);
            if (school != null)
            {
                return Ok(school);
            }
            return NotFound();
        }

        // GET api/<controller>?name=jova
        // [Route("api/schools?year=1975")]
        // [HttpGet]
        public IHttpActionResult GetSearchYoungerByYear(int year)
        {
            var schools = Schools.Where(x => x.Year > year);
            if (schools != null)
            {
                return Ok(schools);
            }
            return BadRequest();
        }

        // POST api/<controller>
        // [Route("api/schools")]
        // [HttpPost]
        public IHttpActionResult Post(School school)
        {
            if (school != null)
            {
                Schools.Add(school);
                return Ok(Schools);
            }
            return BadRequest();
        }

        // PUT api/<controller>/
        // [Route("api/schools/")]
        // [HttpPut]
        public IHttpActionResult PutAll(List<School> schools)
        {
            if (schools != null)
            {
                Schools.Clear();
                Schools = schools;
                return Ok(Schools);
            }
            return BadRequest();
        }

        // PUT api/<controller>/5
        // [Route("api/schools/5")]
        // [HttpPut]
        public IHttpActionResult Put(int id, School newSchool)
        {
            var oldSchool = Schools.FirstOrDefault(x => x.Id == id);
            if (oldSchool != null && newSchool != null)
            {
                //var index = Schools.IndexOf(oldSchool);
                //if (index != -1)
                //    Schools[index] = newSchool;
                oldSchool.Name = newSchool.Name;
                oldSchool.Place = newSchool.Place;
                oldSchool.Year = newSchool.Year;
                return Ok(oldSchool);
            }
            return BadRequest();
        }

        // DELETE api/<controller>/5
        // [Route("api/schools/5")]
        // [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var school = Schools.FirstOrDefault(x => x.Id == id);
            if (school != null)
            {
                Schools.Remove(school);
                return Ok(Schools);
            }
            return BadRequest();
        }
    }
}
