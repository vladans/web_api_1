﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int Year { get; set; }

        public Employee(int id, string name, string lastName, int year)
        {
            this.Id = id;
            this.Name = name;
            this.LastName = lastName;
            this.Year = year;
        }
    }
}