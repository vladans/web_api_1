﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class EmployeesController : ApiController
    {
        static IList<Employee> Emps = new List<Employee>()
        {
            new Employee(1, "Nikola", "Tesla", 1984),
            new Employee(2, "Novak", "Djokovic", 1990),
            new Employee(3, "Donald", "Trumph", 1954)
        };

        // GET api/<controller>
        // [Route("api/employees")]
        // [HttpGet]
        public IHttpActionResult GetAll()
        {
            if (Emps != null)
            {
                return Ok(Emps);
            }

            return NotFound();
        }

        // GET api/<controller>/id
        // [Route("api/employees/id")]
        // [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            var employee = Emps.FirstOrDefault(e => e.Id == id);
            if (employee != null)
            {
                return Ok(employee);
            }
            return NotFound();
        }

        // GET api/<controller>?name=jova
        // [Route("api/employees?name=jova")]
        // [HttpGet]
        public IHttpActionResult GetSearchByName(string name)
        {
            //var employees = Emps.Where(e => e.Name.ToLower()
            //                    .Contains(name.ToLower()))
            //                    .OrderBy(e => e.Id);
            var employee = Emps.FirstOrDefault(e => e.Name == name);
            if (employee != null)
            {
                return Ok(employee);
            }
            return BadRequest();
        }

        // POST api/<controller>
        // [Route("api/employees")]
        // [HttpPost]
        public IHttpActionResult Post(Employee employee)
        {
            if (employee != null)
            {
                Emps.Add(employee);
                return Ok(Emps);
            }
            return BadRequest();
        }

        // PUT api/<controller>/
        // [Route("api/employees/")]
        // [HttpPut]
        public IHttpActionResult PutAll(List<Employee> newEmps)
        {
            if (newEmps != null)
            {
                Emps.Clear();
                Emps = newEmps;
                return Ok(Emps);
            }
            return BadRequest();
        }

        // PUT api/<controller>/5
        // [Route("api/employees/5")]
        // [HttpPut]
        public IHttpActionResult Put(int id, Employee newEmp)
        {
            var tmpEmp = Emps.FirstOrDefault(e => e.Id == id);
            if (tmpEmp != null && newEmp != null)
            {
                //var index = Emps.IndexOf(tmpEmp);
                //if (index != -1)
                //    Emps[index] = newEmp;
                tmpEmp.Name = newEmp.Name;
                tmpEmp.LastName = newEmp.LastName;
                tmpEmp.Year = newEmp.Year;
                return Ok(tmpEmp);
            }
            return BadRequest();
        }

        // DELETE api/<controller>/5
        // [Route("api/employees/5")]
        // [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var tmpEmp = Emps.FirstOrDefault(e => e.Id == id);
            if (tmpEmp != null)
            {
                Emps.Remove(tmpEmp);
                return Ok(Emps);
            }
            return BadRequest();
        }
    }
}
